<?php
/**
 * @name Melodic\DI\Module
 * 		Maps an interface to an implementation, and holds the instantiated object
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\DI
{
	class Module
	{
		#region public properties

		public $implementation;
		public $singleton;
		public $instance;

		#endregion

		#region constructor

		/**
		 * Create a module map
		 * @param $implementation - The name of the class implementation
		 * @param bool $singleton - A boolean indicating whether to use the instantiated class as a singleton
		 */
		public function __construct($implementation, $singleton = false, $instance = null)
		{
			$this->implementation = $implementation;
			$this->singleton = $singleton;
			$this->instance = $instance;
		}

		#endregion
	}
}
?>