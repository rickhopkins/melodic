<?php
/**
 * @name Melodic\DI\Interfaces\iInject
 * 		A contract for dependency injection
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\DI\Interfaces
{
	interface iInject
	{
		static function instantiateByInjection($params = []);
	}
}
?>