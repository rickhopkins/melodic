<?php
/**
 * @name Melodic\DI\Personality
 * 		Adds functionality to classes that are injectable, or receive injectables
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\DI\Personality
{
	use Melodic\DI;

	trait Injection
	{
		public static function instantiateByInjection($params = [])
		{
			return DI::instantiate(get_called_class(), $params);
		}
	}
}
?>