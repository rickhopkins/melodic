<?php
/**
 * @name Melodic\Core
 * 		Extension functions and configuration for Melodic
 * @author Rick Hopkins
 * @package Melodic
 */

namespace
{
	/***********************************************************************
	 * Common Functions
	 ***********************************************************************/
	
	/** register the Melodic auto load function */
	spl_autoload_register("melodic_autoload");
	
	/** register the document root as part of the include path */
	register_include_path($_SERVER["DOCUMENT_ROOT"]);
	
	/** set the default exception handler */
	// set_error_handler(array("\\Melodic\\MVC\\MVCController", "Error"));
	// set_exception_handler(array("\\Melodic\\MVC\\MVCController", "Exception"));

    /**
     * Auto load the given class
     * @param string $class - The class name being instantiated
     * @return void
     */
	function melodic_autoload($class) 
	{
		/** check for root file first */
		$class = strtolower($class);
		if (is_readable($class.".php")) $fileName = $class.".php";
		else {
			/** get the file name */
			$fileName = str_replace("\\", DIRECTORY_SEPARATOR, str_replace("_", DIRECTORY_SEPARATOR, $class)).".php";
		}

		/** require the file */
		require_once $fileName;
	}

    /**
     * Add to the include path
     * @param string $path - The path to add to the include path
     * @return void
     */
	function register_include_path($path)
	{
		set_include_path(get_include_path().":".$path);
	}

    /**
     * Return the benchmark time
     * @param int $time1 - The first time mark in microseconds
     * @param int $time2 - The second time mark in microseconds
     * @return string - The benchmark process time
     */
	function bench_mark($time1, $time2)
	{
		$benchMark = number_format($time2 - $time1, 5);
		if ($benchMark < 60) return $benchMark." seconds";
		else return number_format(($benchMark / 60), 3)." minutes";
	}

    /**
     * Test a user agent string to see what type of browser it is
     * @param string $test - The string to test the browser agent string for
     * @return boolean - A boolean flag indicating whether the Browser User Agent string contains the test string
     */
	function browser_test($test)
	{
		if (str_contains($_SERVER["HTTP_USER_AGENT"], $test)) return true;
		else return false;
	}

    /**
     * Kill the script and print_r whatever var is passed
     * @param mixed $var - The object to dump to the screen
     * @param boolean $pre - A boolean flag indicating whether to include the <pre></pre> tags
     * @return void
     */
	function kill($var, $pre = true)
	{
		if ($pre) print "<pre>";
		print_r($var);
		if ($pre) print "</pre>";
		die;
	}

    /**
     * Redirect the page
     * @param string $url - The url to redirect to
     * @return void
     */
	function redirect($url = "/")
	{
		header("Location: ".$url);
		die;
	}

	/**
	 * Send a page not found response
	 * @return void
	 */
	function pageNotFound()
	{
		/** send page not found */
		header("HTTP/1.0 404 Not Found");
		die;
	}

	/**
	 * Get the current page url
	 * @return string
	 */
	function get_current_url()
	{
		$url = "http".(isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on" ? "s" : "")."://";
		$url .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		return $url;
	}
	
	/**
     * Convert data to serialized base64 encoded string
     * @param mixed $data - The data to serialized and base 64 encoded
     * @return string - The data as a serialized base64 encoded string
     */
	function serial64_encode($data)
	{
		return base64_encode(serialize($data));
	}
	
	/**
	 * Convert from serialized base64 encoded string to data
	 * @param string $data - The serialized base 64 encoded string representing data
	 * @return mixed - The deserialized data
	 */
	function serial64_decode($data)
	{
		return unserialize(base64_decode($data));
	}

    /**
     * Create a completely unique id
     * @param string $prefix - A string prefix
     * @return string - A string id
     */
	function new_id($prefix = null)
	{
		return $prefix.md5(uniqid(rand(), true));
	}
	
	/**
	 * Get all headers and return key/value array
	 * @return array
	 */
	function getHeaders()
	{
		/** get the headers */
		$headers = headers_list();
		
		/** cycle through headers to create key/value array */
		$newHeaders = array();
		foreach ($headers as $header){
			$header = explode(": ", $header);
			$newHeaders[$header[0]] = $header[1];
		}
		
		/** return the new headers */
		return $newHeaders;
	}
	
	/***********************************************************************
	 * Array Extension Functions
	 ***********************************************************************/
	
	/**
	 * Copy an array
	 * @param array $array - The array to copy
	 * @return array - The new array
	 */
	function array_copy($array)
	{
		$array = serialize($array);
		$array = unserialize($array);
		return $array;
	}
	
	/**
	 * Sort a complex array by the given array key
	 * @param array $array - The array to sort
	 * @param string $order - The order to sort in (asc - or - desc)
	 * @param string $key - A string indicating a key to sort a complex array by
	 * @return array - The sorted array
	 */
	function array_sort($array, $order = "asc", $key = null)
	{
		if (!is_array($array) || count($array) < 1) return $array;
		if ($key == null){
			if ($order == "asc") return asort($array);
			else return arsort($array);
		}
		foreach ($array as $k => $vals){
			if (strtotime($vals[$key])) $new[$k] = strtotime($vals[$key]);
			else $new[$k] = $vals[$key];
		}
		if ($order == "asc") asort($new);
		else arsort($new);
		$keys = array_keys($new);
        $return = array();
		foreach ($keys as $k) $return[$k] = $array[$k];
		return $return;
	}
	
	/**
	 * Remove duplicates from an array
	 * @param array $array - The array to clean
	 * @param string $key - A string indicating a key to search for duplicate values
	 * @return array - The cleaned array
	 */
	function array_clean($array, $key = null)
	{
		if (!is_array($array) || count($array) < 1) return $array;
		if ($key == null) return array_unique($array);
        $tmp = array();
		foreach ($array as $a) $tmp[$a[$key]] = $a;
        $new = array();
		foreach ($tmp as $a) $new[] = $a;
		return $new;
	}
	
	/**
	 * Return the highest value in an array
	 * @param array $array - The array to search
	 * @return mixed - The value that was decided to be the highest
	 */
	function array_max($array)
	{
		rsort($array, SORT_NUMERIC);
		return $array[0];
	}
	
	/**
	 * Return the lowest value in an array
	 * @param array $array - The array to search
	 * @return mixed - The value that was decided to be the lowest
	 */
	function array_min($array)
	{
		sort($array, SORT_NUMERIC);
		return $array[0];
	}
	
	/***********************************************************************
	 * String Extension Functions
	 ***********************************************************************/

    /**
     * Test a string for another string
     * @param string $hayStack - The string to search
     * @param string $needle - The string to search for
     * @return boolean - A boolean flag indicating whether the haystack string contains the needle string
     */
	function str_contains($hayStack, $needle)
	{
		if (strpos($hayStack, $needle) !== false) return true;
		else return false;
	}

    /**
     * Escape a string value and return
     * @param string $str - The string to clean
     * @return string - The cleaned string
     */
	function str_clean($str)
	{
		return htmlentities($str, ENT_QUOTES, "UTF-8");
	}
	
	/***********************************************************************
	 * Date Extension Functions
	 ***********************************************************************/

    /**
     * Return the days between two dates
     * @param string $beg - The beginning date string
     * @param string $end - The ending date string
     * @param string $format - The format to follow for return dates
     * @return array - An array of date strings between the 2 given dates
     */
	function date_between($beg, $end, $format = "Y-m-d")
	{
		$dates = array();
		$beg = strtotime($beg);
		$end = strtotime($end);
		while ($beg <= $end){
			$dates[] = date($format, $beg);
			$beg = strtotime("+1 days", $beg);
		}
		return $dates;
	}

    /**
     * Return true if the first date is greater than the second, false otherwise
     * @param string $first - The date string to compare
     * @param string $second - The date string to compare against
     * @return boolean - A boolean flag indicating whether the first date is later than the second
     */
	function date_later($first, $second)
	{
		$first = strtotime($first);
		$second = strtotime($second);
		return $first > $second;
	}
	
	/**
	 * Return the greater of 2 dates
	 * @param string $first - The first date string
	 * @param string $second - The second date string
	 * @return string - The greater of the 2 date strings
	 */
	function date_greater($first, $second)
	{
		$firstU = strtotime($first);
		$secondU = strtotime($second);
		if ($firstU > $secondU) return $first;
		else return $second;
	}
}
?>