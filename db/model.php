<?php
/**
 * @name Melodic\DB\Model
 * 		An abstract class to define a data model
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\DB
{
	abstract class Model
	{
		/** private properties */
		private $_key;
		private $_table;
		private $_properties;

		/**
		 * Initialize a new Model object
		 * @param array $data - An array of key/value pair to populate the data model
		 * @return \Melodic\DB\Model - The Model object
		 */
		public function __construct($data = null)
		{
			/** setup private variables */
			$self = new \ReflectionClass($this);
			$this->_properties = $self->getProperties(\ReflectionProperty::IS_PUBLIC);

			/** set table name */
			$className = explode("\\", get_class($this));
			$this->_table = end($className);

			/** set the id based on the first property */
			$this->_key = $this->_properties[0]->name;

			/** check for data */
			if ($data != null && is_array($data)){
				/** cycle through data to set values */
				foreach ($data as $property => $value){
					/** set the value */
					if ($self->getProperty($property)){
						/** get the data type */
						$type = gettype($this->$property);

						/** try to convert to type */
						if ($type != "NULL" && $type != "unknown type") settype($value, $type);

						/** set the type */
						$this->$property = $value;
					}
				}
			}

			/** return the Model */
			return $this;
		}

		/**
		 * Set a property value
		 * @param $property
		 * @return mixed
		 */
		public function __get($property)
		{
			return $this->$property;
		}

		/**
		 * Set a property value
		 * @param $property
		 * @param $value
		 * @return $this
		 */
		public function __set($property, $value)
		{
			$this->$property = $value;
			return $this;
		}

		/**
		 * Create an instance of a model with the passed data
		 * @param $array
		 * @return Model
		 */
		static function create($array = null)
		{
			/** get the class name and return a new object */
			$className = get_called_class();
			return new $className($array);
		}
	}
}
?>