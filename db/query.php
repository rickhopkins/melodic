<?php
/**
 * @name Melodic\DB\Query
 * 		Handles execution of queries
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\DB
{
	class Query
	{
		/** public properties */
		public $statement;
		public $context;
		public $rs;
		public $params = array();

        /**
         * Initialize a new Query object
         * @param \Melodic\DB\Context $context - The database context object to use
         * @param string $statement - A string query statement to be executed
         * @return \Melodic\DB\Query - The Melodic\DB\Query object
         */
		public function __construct(Context $context, $statement = null)
		{
			/** set properties */
			$this->context = $context;
			$this->statement = $statement;
			
			/** return the Query object */
			return $this;
		}

        /**
         * Get the actual query statement being executed
         * @return string - The string query statement being executed
         */
		public function getStatement()
		{
			return $this->statement;
		}

        /**
         * Set the actual query statement to be executed
         * @param string $statement - The query string to be executed
         * @return \Melodic\DB\Query - The Query object
         */
		public function setStatement($statement)
		{
			$this->statement = $statement;
			return $this;
		}

        /**
         * Add a bound parameter
         * @param \Melodic\DB\Param $param - The bound parameter
         * @return \Melodic\DB\Query - The Query object
         */
		public function addParam(Param $param)
		{
			/** add the parameter */
			array_push($this->params, $param);
			
			/** return the Query */
			return $this;
		}
		
		/**
		 * Add multiple bound parameters
		 * @param array $params - The parameters to add
		 * @return \Melodic\DB\Query
		 */
		public function addParams($params){
			/** cycle through parameters */
			foreach ($params as $param){
				$this->addParam($param);
			}
			
			/** return Query */
			return $this;
		}

        /**
         * Execute a query
         * @param string $statement - The query string to be executed
         * @throws Exception
         * @return \Melodic\DB\Query - The Query object
         */
		public function execute($statement = null)
		{
			/** set the statement */
			if ($statement != null) $this->statement = $statement;
			
			/** check that a statement exists */
			if ($this->statement == null) throw new Exception("Melodic\\DB\\Query Error: No Query Statement Supplied");
			
			/** check for bound parameters or columns */
			if (is_array($this->params) && count($this->params) > 0){
				/** prepare the query and bind params */
				$this->rs = $this->context->conn->prepare($this->statement);
				foreach ($this->params as $p) $p->bind($this->rs);
				
				/** execute the prepared query */
				$this->rs->execute();
			} else {
				/** execute the query */
				$this->rs = $this->context->conn->query($this->statement);
			}
			
			/** return the Query object */
			return $this;
		}

        /**
         * Get the query results as an array of rows
         * @return array - The results of the query as an array of rows
         */
		public function asRows()
		{
			/** setup an array for returning */
			$rows = array();
			
			/** check to see if the query has been executed already */
			if ($this->rs == null) $this->execute();
			
			/** fill the rows array */
			while ($row = $this->rs->fetch(\PDO::FETCH_ASSOC)){
				array_push($rows, $row);
			}
			
			/** return the rows */
			return $rows;
		}

		/**
		 * Get the query results as an array of models of the given type
		 * @param string $model - The model name to use
		 * @param array $args - An array of arguments to pass to the model class constructor
		 * @throws Exception
		 * @return array - The results of the query as an array of models of the given type
		 */
		public function asModels($model, $args = null)
		{
			/** setup an array for returning */
			$models = array();
			
			/** check to see if the query has been executed already */
			if ($this->rs == null) $this->execute();
			
			/** set the fetch mode */
			$this->rs->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, $model, $args);
			
			/** fill the models array */
			while ($m = $this->rs->fetch(\PDO::FETCH_CLASS)){
				array_push($models, $m);
			}
			
			/** return the models */
			return $models;
		}
		
		/**
		 * Get the number of rows affected by a query
		 * @return int - The number of rows effected by the last query
		 */
		public function rowCount()
		{
			if ($this->rs != null) return $this->rs->rowCount();
			else return null;
		}
	}
}
?>