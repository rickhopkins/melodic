<?php
/**
 * @name Melodic\DB\Traits\CRUD
 *      A class for simple CRUD operations
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\DB
{
	class CRUD
	{
		/** private properties */
		private $context;

		/**
		 * Initialize the CRUD
		 * @param Context $context
		 * @return CRUD
		 */
		public function __construct(Context $context)
		{
			$this->context = $context;
			return $this;
		}

		/**
		 * Insert a record
		 * @param Model $model - The record to add
		 * @return Model
		 */
		public function create(Model $model)
		{
			/** get the model id */
			$id = $model->_key;

			/** get the properties */
			$properties = $model->_properties;
			$fields = array();
			$params = array();
			foreach ($properties as $prop){
				/** get the property name */
				$name = $prop->getName();

				/** make sure it's not the id field */
				if ($name != $id){
					/** add to the fields and params array */
					array_push($fields, $name);
					array_push($params, new Param(":".$name, $model->$name, $this->getParamType($model->$name)));
				}
			}

			/** begin the sql statement */
			$query = new Query($this->context, sprintf("INSERT INTO %s (%s) VALUES (:%s)", $model->_table, implode(", ", $fields), implode(", :", $fields)));
			$query->addParams($params);
			$query->execute();

			/** get the last inserted id and add to model */
			$model->$id = $this->context->lastInsertID();

			/** return the model */
			return $model;
		}

		/**
		 * Read data
		 * @param Model $model
		 * @param $id
		 * @param string $field
		 * @return Model or array
		 */
		public function read(Model $model, $id = null, $field = null)
		{
			/** query the database */
			$query = new Query($this->context, sprintf("SELECT * FROM %s", $model->_table));
			if ($id != null) $query->statement .= sprintf(" WHERE %s = %d", ($field == null ? $model->_key : $field), $id);

			/** execute query */
			$results = $query->execute()->asModels(get_class($model));
			return (count($results) > 0 ? (count($results) > 1 ? $results : $results[0]) : null);
		}

		/**
		 * Update a record
		 * @param Model $model - The record to update
		 * @return Model
		 */
		public function update(Model $model)
		{
			/** get the model id */
			$id = $model->_key;

			/** start the sql statement */
			$sql = sprintf("UPDATE %s SET ", $model->_table);

			/** get the properties */
			$params = array();
			foreach ($model->_properties as $prop){
				/** get the property name */
				$name = $prop->getName();

				/** make sure it's not the id field */
				if ($name != $id) $sql .= sprintf("%s = :%s", $name, $name);

				/** add the parameter */
				array_push($params, new Param(":".$name, $model->$name, $this->getParamType($model->$name)));
			}

			/** add where clause */
			$sql .= sprintf("WHERE %s = :%s", $id, $id);

			/** begin the sql statement */
			$query = new Query($this->context, $sql);
			$query->addParams($params);
			$query->execute();

			/** return the model */
			return $model;
		}

		/**
		 * Remove a record from the database
		 * @param Model $model
		 * @return Repository
		 */
		public function delete(Model $model)
		{
			/** query the database */
			$query = new Query($this->context, sprintf("DELETE FROM %s WHERE %s = %d", $model->_table, $model->_key, $model->{$model->_key}));
			$query->execute();
			return $this;
		}

		/**
		 * Find data
		 * @param Model $model
		 * @param array $query
		 * @return Model or array
		 */
		public function find(Model $model, $query = array())
		{
			/*
			eq 	Equal	                         /User?query=Username eq rhopkins
			ne	    Not equal	                     /User?query=Username ne rhopkins
			gt	    Greater than	                 /User?query=UserID gt 1
			ge	    Greater than or equal	 /User?query=UserID ge 1
			lt	    Less than	                     /User?query=UserID lt 5
			le	    Less than or equal	         /User?query=UserID le 5
			lk		Like match					 /User?query=Username lk %rhopkins%
			and	Logical and	                 /User?query=Username eq rhopkins and UsernID lt 5
			or 	Logical or	                     /User?query=Username eq rhopkins or UserID lt 5
			*/

			/** create array to hold query bound parameters */
			$params = array();

			/** start the query */
			$sql = sprintf("SELECT * FROM %s [CRITERIA]", $model->_table);

			/** see if a orderby is specified */
			if (array_key_exists("orderby", $query)){
				/** add the order by clause */
				$sql .= sprintf("ORDER BY %s ", $query["orderby"]);
			}

			/** see if a limit is imposed */
			if (array_key_exists("limit", $query)){
				/** add the LIMIT */
				$sql .= sprintf("LIMIT %s", $query["limit"]);
			}

			/** create critera string */
			$criteriaStr = "";
			if (array_key_exists("query", $query)){
				/** get the query */
				$query["query"] = str_replace(array(" eq ", " ne ", " gt ", " ge ", " lt "," le ", " lk "), array(" = ", " != ", " > ", " >= ", " < ", " <= ", " LIKE "), $query["query"])." ";

				/** find defined sections */
				preg_match_all("/(\\S*?) (=|!=|>|>=|<|<=|LIKE) (.*)/", $query["query"], $matches);

				/** check for matches */
				if (count($matches) > 0){
					/** cycle through matches to modify the query and create bound parameters */
					foreach ($matches[0] as $key => $match){
						/** get the field name */
						$oldClause = $matches[0][$key];
						$field = $matches[1][$key];
						$operator = $matches[2][$key];
						$val = str_replace("'", "", $matches[3][$key]);
						if ($operator == 'LIKE') $val = "%".$val."%";

						/** new string */
						$newClause = sprintf("%s %s :%s", $field, $operator, $field);
						array_push($params, new Param(sprintf(":%s", $field), $val, $this->getParamType($model->$field)));

						/** replace the old match string with the new match string */
						$query["query"] = str_replace($oldClause, $newClause, $query["query"]);
					}

					/** setup criteria string */
					$criteriaStr = "WHERE ".$query["query"];
				}
			}

			/** query the database */
			$queryObj = new Query($this->context, str_replace("[CRITERIA]", $criteriaStr, $sql));
			if (count($params) > 0) $queryObj->addParams($params);

			// kill($queryObj);

			/** execute query */
			$results = $queryObj->execute()->asModels(get_class($model));
			return (count($results) > 0 ? (count($results) > 1 ? $results : $results[0]) : null);
		}

		/**
		 * Get the PDO Param type for the given value
		 * @param mixed $value - The value to get the type of
		 * @return int
		 */
		private function getParamType($value)
		{
			/** get the data type */
			$type = gettype($value);
			switch ($type){
				case "boolean": $type = \PDO::PARAM_BOOL; break;
				case "integer": $type = \PDO::PARAM_INT; break;
				case "NULL": $type = \PDO::PARAM_NULL; break;
				default: $type = \PDO::PARAM_STR; break;
			}

			/** return type */
			return $type;
		}
	}
}
?>