<?php
/**
 * @name Melodic\DB\Relationship
 *      Class to define a relationship between models
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\DB
{
	class Relationship
	{
		/** public properties */
		public $model;
		public $name;
		public $key;
		public $type = "1:M"; // 1:1 (One To One), 1:M (One To Many)
		const ONE_TO_ONE = "1:1";
		const ONE_TO_MANY = "1:M";

		/**
		 * Initialize the relationship
		 * @param string $model - The model name of the related entity
		 * @param string $key - The field that makes the relationship
		 * @param string $type - Is this a One To One (1:1), or One To Many (1:M) relationship
		 * @return \Melodic\DB\Relationship
		 */
		public function __construct($model, $key, $type = Relationship::ONE_TO_MANY)
		{
			/** set table name */
			$className = explode("\\", $model);
			$this->name = end($className);

			/** set values */
			$this->model = $model::create();
			$this->key = $key;
			$this->type = $type;

			/** return Relationship */
			return $this;
		}
	}
}