<?php
/**
 * @name Melodic\DB\Param
 * 		A class that handles binding parameters to database queries
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\DB
{
	class Param
	{
		/** private properties */
		private $parameter;
		private $value;
		private $type;

        /**
         * Initialize a new Param
         * @param string $parameter - The string parameter name
         * @param mixed $value - The value of the parameter
         * @param constant $type - The type of data
         * @return \Melodic\DB\Param
         */
		public function __construct($parameter, $value, $type)
		{
			/** set properties */
			$this->parameter = $parameter;
			$this->value = $value;
			$this->type = $type;
			
			/** return Param */
			return $this;
		}
		
		/**
		 * Get a property (Magic Method)
		 * @param string $property - The property to retrieve
		 * @return mixed - The value of the Param property
		 */
		public function __get($property)
		{
			return $this->$property;
		}

        /**
         * Set a property (Magic Method)
         * @param string $property - The property to set
         * @param mixed $value - The value to set
         * @return \Melodic\DB\Param
         */
		public function __set($property, $value)
		{
			$this->$property = $value;
			return $this;
		}

        /**
         * Bind the parameter to the given prepared query
         * @param \PDOStatement $PDOStatement - The PDO Statement to bind params to
         * @return boolean - A boolean flag indicating whether bind was successful
         */
		public function bind(\PDOStatement $PDOStatement)
		{
			if ($PDOStatement->bindValue($this->parameter, $this->value, $this->type)) return true;
			else return false;
		}
	}
}
?>