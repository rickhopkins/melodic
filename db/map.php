<?php
/**
 * @name Melodic\DB\Map
 *      Class to define a mapping between model and database table
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\DB
{
	class Map
	{
		/** public properties */
		public $model;
		public $instance;
		public $table;
		public $key;
		public $constraints = array();
		public $relationships = array();

		/**
		 * Initialize the Map
		 * @param string $model - The string name of the model to map
		 * @return Map
		 */
		public function __construct($model)
		{
			$this->setModel($model);
			$this->setTable($this->instance->_table);
			$this->setKey($this->instance->_key);
			return $this;
		}

		/**
		 * Set the Model to use
		 * @param string $model
		 * @return \Melodic\DB\Map
		 */
		public function setModel($model)
		{
			$this->model = $model;
			$this->instance = $model::create();
			return $this;
		}

		/**
		 * Get the model
		 * @return string
		 */
		public function getModel()
		{
			return $this->model;
		}

		/**
		 * Set the database table name
		 * @param string $table
		 * @return \Melodic\DB\Map
		 */
		public function setTable($table)
		{
			$this->table = $table;
			return $this;
		}

		/**
		 * Get the database table
		 * @return string
		 */
		public function getTable()
		{
			return $this->table;
		}

		/**
		 * Set the key field name
		 * @param string $key - The property name that serves as primary key
		 * @return \Melodic\DB\Map
		 */
		public function setKey($key)
		{
			if ($this->hasField($key)) $this->key = $key;
			return $this;
		}

		/**
		 * Get the key
		 * @return string
		 */
		public function getKey()
		{
			return $this->key;
		}

		/**
		 * Check if the Map has the specified relationship, by name
		 * @param string $name
		 * @return bool
		 */
		public function hasRelationship($name)
		{
			/** get the relationship index */
			if ($this->relationshipIndex($name) == -1) return false;
			else return true;
		}

		/**
		 * Get the index of the specified relationship
		 * @param string $name - The Relationship name
		 * @return int
		 */
		public function relationshipIndex($name)
		{
			/** cycle through relationships to check */
			for ($i = 0; $i < count($this->relationships); $i++){
				if ($this->relationships[$i]->name == $name) return $i;
			}

			/** return the index */
			return -1;
		}

		/**
		 * Add a relationship to the relationships array
		 * @param Relationship $relationship
		 * @return Map
		 */
		public function addRelationship(Relationship $relationship)
		{
			/** add the relationship */
			if (!$this->hasRelationship($relationship->name)){
				array_push($this->relationships, $relationship);
			}

			/** return Map */
			return $this;
		}

		/**
		 * Add relationships from array
		 * @param $relationships
		 * @return Map
		 */
		public function setRelationships($relationships)
		{
			/** cycle through relationships to add */
			foreach ($relationships as $rel){
				$this->addRelationship($rel);
			}

			/** return Map */
			return $this;
		}

		/**
		 * Get the relationships array
		 * @return array
		 */
		public function getRelationships()
		{
			return $this->relationships;
		}

		/**
		 * Check the Model for the specified field
		 * @param string $field - The field name to check for
		 * @throws \Exception
		 * @return boolean
		 */
		public function hasField($field)
		{
			/** check that the model has the field */
			if (!isset($this->instance->$field)){
				/** throw an exception */
				throw new \Exception(sprintf("Field '%s' does not exist on model.", $field));
			}
			
			/** return true */
			return true;
		}
		
		/**
		 * Add a field constraint
		 * @param string $field - The name of the field to add the constraint to
		 * @param string $constraint - The constraint name
		 * @param mixed $value - The value to set the constraint to
		 * @return Map
		 */
		public function addConstraint($field, $constraint, $value)
		{
			/** check that the model has the field */
			if ($this->hasField($field)){
				/** check to see if the field has any constraints yet */
				if (!isset($this->constraints[$field])) $this->constraints[$field] = array();
				
				/** set the field constraint */
				$this->constraints[$field][$constraint] = $value;
			}
			
			/** return map */
			return $this;
		}
		
		/**
		 * Clear the constraints, specify a field name to only clear the constraints tied to that field
		 * @param string $field (optional) - The optional field name to clear constraints for
		 * @return Map
		 */
		public function clearConstrains($field = null)
		{
			/** check for field */
			if ($field != null && isset($this->constraints[$field])) unset($this->constraints[$field]);
			else $this->constraints = array();
			return $this;
		}
		
		/**
		 * Set the constraints
		 * @param array $constraints - Set the constraints array
		 * @return Map
		 */
		public function setConstraints($constraints)
		{
			/** cycle through constraints to add */
			foreach ($constraints as $field => $constraint){
				/** get the key of the constraint */
				$key = key($constraint);
				$value = $constraint[$key];
				
				/** add the constraint */
				$this->addConstraint($field, $key, $value);
			}
			
			/** return map */
			return $this;
		}
		
		/**
		 * Get the constraints
		 * @return array
		 */
		public function getConstraints()
		{
			return $this->constraints;
		}
	}

	/**
	 * @name Melodic\DB\Constraint
	 *      Class to define constant constraints on model properties
	 * @author Rick Hopkins
	 * @package Melodic
	 */
	class Constraint
	{
		const MaxLength = "maxLength";
		const MaxValue = "maxValue";
		const MinValue = "minValue";
	}
}