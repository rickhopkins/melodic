<?php
/**
 * @name Melodic\DB\Repository
 *      A class that allows for interaction between a data map, a model, and the database
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\DB
{
	class Repository
	{
		/** public properties */
		public $crud;
		public $map;

		/**
		 * Initialize the Repository
		 * @param Context $context
		 * @param Map $map
		 * @return Repository
		 */
		public function __construct(Context $context, Map $map)
		{
			$this->crud = new CRUD($context);
			$this->map = $map;
			return $this;
		}

		/**
		 * Get a record by it's by id
		 * @param int $id - The id of the desired record
		 * @return Model
		 */
		public function get($id)
		{
			/** query the database */
			$result = $this->crud->read($this->map->instance, $id);

			/** check the result */
			if ($result != null){
				/** get the record */
				$this->getRelationships($result);

				/** return the record */
				return $result;
			} else return array();
		}

		/**
		 * Get all records of specified type
		 * @param array $query
		 * @return array
		 */
		public function getAll($query = array())
		{
			/** query the database */
			if (count($query) > 0) $results = $this->crud->find($this->map->instance, $query);
			else $results = $this->crud->read($this->map->instance);

			/** check for null */
			if ($results == null) return [];

			/** adjust the array if necessary */
			if (!is_array($results)) $results = [$results];

			/** cycle through results and get the relationships */
			foreach ($results as $result){
				/** get the record */
				$this->getRelationships($result);
			}

			/** return the results */
			return $results;
		}

		/**
		 * Save a record
		 * @param Model $model - The record to save
		 * @return Model
		 */
		public function save(Model $model)
		{
			/** apply the constraints the model data */
			$model = $this->applyConstraints($model);

			/** get the id field */
			$id = $model->_key;

			/** check for id */
			if ($model->$id == null || $model->$id == 0){
				/** add the record */
				$model = $this->crud->create($model);
			} else {
				/** update the record */
				$model = $this->crud->update($model);
			}

			/** return the record */
			return $model;
		}

		/**
		 * Save multiple records
		 * @param array $models - The records to save
		 * @return array
		 */
		public function saveAll($models)
		{
			/** save the records */
			foreach ($models as $key => $model){
				$models[$key] = $this->save($model);
			}

			/** return the records */
			return $models;
		}

		/**
		 * Delete a record
		 * @param int $id - The id of the record to be deleted
		 * @return Repository
		 */
		public function delete($id)
		{
			/** get the record first */
			$record = $this->crud->read($this->map->model, $id);

			/** query the database */
			$this->crud->delete($record);
			return $this;
		}

		/**
		 * Delete multiple records
		 * @param array $ids - The ids of the records to be deleted
		 * @return Repository
		 */
		public function deleteAll($ids)
		{
			/** cycle through ids to delete */
			foreach ($ids as $id) {
				$this->delete($id);
			}

			/** return the factory */
			return $this;
		}

		/**
		 * Get the related data
		 * @param Model $record - The data record to get related data for
		 * @return Model
		 */
		private function getRelationships($record)
		{
			/** cycle through relationships to get related data */
			foreach ($this->map->getRelationships() as $rel){
				/** get the field name and key field name */
				$name = $rel->name;
				$value = $record->{$rel->key};

				/** decide which type of relationship it is */
				if ($rel->type == Relationship::ONE_TO_ONE){
					/** get the related record */
					$record->$name = $this->crud->read($rel->model, $value);
				} elseif ($rel->type == Relationship::ONE_TO_MANY) {
					/** get the related records */
					$record->$name = $this->crud->read($rel->model, $value, $rel->key);
				}
			}
		}

		/**
		 * Apply the constraints to the model data
		 * @param Model $model
		 * @throws
		 * @return Model
		 */
		private function applyConstraints(Model $model)
		{
			/** cycle through constraints to apply to data */
			foreach ($this->map->getConstraints() as $property => $constraint){
				/** get the constraint key */
				$constraintType = key($constraint);
				$constraintValue = $constraint[$constraintType];

				/** check the type */
				switch ($constraintType){
					case Constraint::MaxLength: $model->$property = substr($model->$property, 0, $constraintValue); break;
					case Constraint::MaxValue:
						if ($model->$property > $constraintValue) throw new \Exception(sprintf("Property value is greater than max value constraint: %s", $property));
						break;
					case Constraint::MinValue:
						if ($model->$property < $constraintValue) throw new \Exception(sprintf("Property value is less than min value constraint: %s", $property));
						break;
				}
			}

			/** return model */
			return $model;
		}
	}
}
?>