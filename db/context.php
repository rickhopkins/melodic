<?php
/**
 * @name Melodic\DB\Context
 * 		Gives us an database context
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\DB
{
	class Context
	{
		/** private properties */
		private $config;

		/** public properties */
		public $conn;

		/**
		 * Initialize a new DB Context
		 * @param $config - An array from config.json. A named context
		 * @throws \Exception
		 * @return \Melodic\DB\Context
		 */
		public function __construct($config)
		{
			/** set the context */
			$this->config = $config;

			/** make the connection */
			$this->connect();
			
			/** return the Context */
			return $this;
		}

		/**
		 * Make a database connection
		 * @throws \Exception
		 * @return \Melodic\DB\Context
		 */
		public function connect()
		{
			/** get the config type for caching the connection */
			$db = $this->config["name"]."_".$this->config["username"];

			/** check for the re-usable connection */
			if (isset($GLOBALS["DB_Connection_".$db])) $this->conn = $GLOBALS["DB_Connection_".$db];
			else {
				/** create and return connection */
				try {
					$this->conn = new \PDO($this->config["driver"], $this->config["username"], $this->config["password"], $this->config["options"]);
					$this->setAttribute(\PDO::ATTR_CASE, \PDO::CASE_NATURAL);
					$this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
					
					/** set a global variable to hold the connection object */
					$GLOBALS["DB_Connection_".$db] = $this->conn;
				} catch (\PDOException $ex){
					/** check for Access Denied exception and hijack so no username/password information is given back in trace */
					if (str_contains(strtolower($ex->getMessage()), "access denied")) throw new \Exception("Database access denied to user");
					else throw $ex;
				}
			}
			
			/** return Context */
			return $this;
		}

        /**
         * Set an attribute on the PDO connection
         * @param int $attribute - A PDO connection attribute
         * @param mixed $value - The value to set to the attribute
         * @return \Melodic\DB\Connection - The Connection object
         */
		public function setAttribute($attribute, $value)
		{
			/** set the attribute */
			$this->conn->setAttribute($attribute, $value);
			
			/** return the Connection */
			return $this;
		}
		
		/**
		 * Get the PDO Error information
		 * @return array - An array of error information
		 */
		public function errorInfo()
		{
			return $this->conn->errorInfo();
		}
		
		/**
		 * Get the last inserted record id
		 * @return mixed - The id of the last record inserted into the database
		 */
		public function lastInsertID()
		{
			return $this->conn->lastInsertId();
		}
	}
}
?>