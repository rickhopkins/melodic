<?php
/**
 * @name Melodic\DB\UnitOfWork
 * 		Creates a Unit Of Work for database operations
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\DB
{
	class UnitOfWork
	{
		/**
		 * Add a Repository
		 * @param string $repoName
		 * @param Repository $repo
		 * @return UnitOfWork
		 */
		public function addRepository($repoName, $repo)
		{
			/** get the repo name */
			$this->$repoName = $repo;

			/** return unit of work */
			return $this;
		}

		/**
		 * @param string $repoName - The repository name
		 * @return Repository
		 */
		public function getRepository($repoName)
		{
			return $this->$repoName;
		}
		
		/**
		 * Remove a repository
		 * @param string $repoName - The name of the repository
		 * @return UnitOfWork
		 */
		public function removeRepository($repoName)
		{
			unset($this->$repoName);
			return $this;
		}
	}
}
?>