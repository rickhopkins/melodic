<?php
/**
 * @name Melodic\DI
 * 		A simple Dependency Injection class that maps Interfaces to Implementations and instantiates the class
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic
{
	use Melodic\DI\Module;

	class DI
	{
		#region private properties

		public $map = [];

		#endregion

		#region constructor

		public function __construct()
		{
			/** check for the di.json */
			if (is_readable("di.json")){
				/** read the file and get the property */
				$config = json_decode(file_get_contents("di.json"), true);
				foreach ($config as $interface => $definition) {
					/** create map */
					$this->map[$interface] = new Module($definition["implementation"], $definition["singleton"]);
				}
			}
		}

		#endregion

		#region public methods

		/**
		 * Create an instance of the desired class
		 * @param $className - The desired class name
		 * @return object - An instance of the given class name
		 */
		public function create($className, $params = [])
		{
			/** get the class and constructor */
			$class = new \ReflectionClass($className);
			$constructor = $class->getConstructor();

			/** check for constructor */
			if ($constructor != null){
				/** get the parameters */
				foreach ($constructor->getParameters() as $param){
					/** check for a class */
					$paramClass = $param->getClass();
					if ($paramClass != null && isset($this->map[$paramClass->name])){
						/** check for a singleton, create and add to params */
						if ($this->map[$paramClass->name]->singleton){
							if ($this->map[$paramClass->name]->instance == null){
								$this->map[$paramClass->name]->instance = $this->create($this->map[$paramClass->name]->implementation);
							}
							$params[] = $this->map[$paramClass->name]->instance;
						} else {
							/** not a singleton, create a new instance */
							$params[] = $this->create($this->map[$paramClass->name]->implementation);
						}
					}
				}
			}

			/** create a new instance */
			$instance = null;
			if ($constructor != null && count($params) > 0) $instance = $class->newInstanceArgs($params);
			else $instance = $class->newInstance();

			/** look for setter injection */
			$this->propertyInject($instance);

			/** return the instance */
			return $instance;
		}

		/**
		 * Search the properties for injectable items and inject
		 * @param $instance - The instance (by reference)
		 * @return void
		 */
		public function propertyInject(&$instance)
		{
			/** get public properties */
			$class = new \ReflectionClass($instance);
			$properties = $class->getProperties(\ReflectionProperty::IS_PUBLIC);

			/** cycle through properties to instantiate and inject dependencies */
			foreach ($properties as $prop){
				/** check the doc comments */
				$propertyInstance = null;
				$annotations = $prop->getDocComment();
				if ($annotations && str_contains($annotations, "@inject")){
					/** find the interface the property uses for mapping */
					preg_match_all('/\/\** @inject ([a-zA-Z0-9_\\\]*?) \*\//', $annotations, $matches);
					$interface = $matches[1][0];

					/** check for di map setting */
					if (isset($this->map[$interface])){
						/** check for singleton and existing instance */
						if ($this->map[$interface]->singleton && $this->map[$interface]->instance != null) $propertyInstance = $this->map[$interface]->instance;
						else $propertyInstance = $this->create($this->map[$interface]->implementation);
					}

					/** set property to instance */
					$instance->{$prop->name} = $propertyInstance;
				}
			}
		}

		#endregion

		#region static methods

		public static function instantiate($className, $params = [])
		{
			/** create a Dependency Injection instance */
			if (!isset($GLOBALS["DI_SERVICE"])) $GLOBALS["DI_SERVICE"] = new DI();
			return $GLOBALS["DI_SERVICE"]->create($className, $params);
		}

		#endregion
	}
}
?>