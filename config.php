<?php
/**
 * @name Melodic\Config
 * 		Retrieves configuration options from config.json
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic
{
	class Config
	{
		/**
		 * Get a config value
		 * @param $key - The config key to be retrieved
		 * @return mixed - The value of the config key
		 */
		public static function get($key)
		{
			/** check for the config.json */
			if (is_readable("config.json")){
				/** try returning the desired value */
				try {
					/** read the file and get the property */
					$contents = json_decode(file_get_contents("config.json"), true);
					if (isset($contents[$key])) return $contents[$key];
					else return null;
				} catch (\Exception $ex){
					/** return null */
					return null;
				}
			} else {
				/** throw an exception */
				throw new \Exception("No config.json file found.");
			}
		}

		/**
		 * Get the client settings for the angular application
		 * @param $moduleName - The name of the angular application
		 * @return string - The script tag with client settings
		 */

		public static function getAngularAppSettings($moduleName)
		{
			/** get the client settings from the config.json */
			$appSettings = Config::get("appSettings");

			/** check for settings */
			if ($appSettings != null){
				/** begin script string */
				$script = "<script type=\"text/javascript\">angular.module('$moduleName').constant('app.settings', {";

				/** cycle thru settings to add */
				$settings = [];
				while (list($key, $val) = each($appSettings)){
					/** add the key/value pair */
					$settings[] = " '$key': '$val' ";
				}

				/** add settings */
				$script .= implode(", ", $settings)."});</script>";

				/** return the string */
				return $script;
			}

			/** return a blank string */
			return "";
		}
	}
}
?>