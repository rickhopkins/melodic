melodic
=======

A simple PHP Framework for rapid site development. Melodic creates an extensible MVC implementation which is modeled after .Net MVC. It also has some similar aspects of WebAPI from .Net, as well as some very basic ORM abilities. Still in development. More to come.
