<?php
/**
 * @name Melodic\MVC\Controller
 * 		Handles all requests
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\MVC
{
	use Melodic\Config;

	class MvcController extends Controller
	{
		/** public properties */
		public $layout = "";
		public $bodyContent = "";
		public $sections = array();
		public $viewBag = array();

		/**
		 * Initialize the controller and parse request
		 * @param Route $route (Melodic/MVC/Route) - The Route object to inform the controller
		 * @return MvcController - The Controller object
		 */
		public function __construct(Route $route)
		{
			/** instantiate the parent */
			parent::__construct($route);

			/** set default layout */
			$this->layout = Config::get("viewsDirectory")."/shared/layout.phtml";

			/** read viewBag properties from the Config */
			$configViewBagValues = Config::get("viewBag");
			if (is_array($configViewBagValues)){
				foreach ($configViewBagValues as $key => $value){
					/** set the value */
					$this->viewBag($key, $value);
				}
			}

			/** return the controller */
			return $this;
		}

		/**
		 * Set / Retrieve a key / value pair from the viewBag
		 * @param string $key - The key to either set or retrieve
		 * @param mixed $value - The value to set for the key
		 * @return mixed
		 */
		public function viewBag($key, $value = null)
		{
			/** check for the key */
			if (isset($this->viewBag[$key])){
				/** check to see if a value was passed */
				if ($value != null){
					/** set the viewBag key/value */
					$this->viewBag[$key] = $value;
					return $this;
				} else {
					/** return the viewBag key / value */
					return $this->viewBag[$key];
				}
			} else {
				/** check to see if a value was passed */
				if ($value != null){
					/** set the viewBag key/value */
					$this->viewBag[$key] = $value;
					return $this;
				} else {
					/** return the viewBag key / value */
					return null;
				}
			}
		}

		/**
		 * Show the specified page
		 * @param mixed $model - The view model to pass
		 * @return void
		 */
		public function view($model = null)
		{
			/** Start buffering */
			ob_start();

			/** process the view code */
			include_once Config::get("viewsDirectory")."/".$this->route->controller."/".$this->route->action.".phtml";

			/** get the output buffer contents */
			$this->bodyContent = ob_get_contents();

			/** stop buffering */
			ob_end_clean();

			/** find defined sections */
			preg_match_all("/#section (.*?)\n(.*?)#end/s", $this->bodyContent, $matches);

			/** for all matches add sections */
			if (count($matches) == 3){
				foreach ($matches[1] as $key => $section){
					/** add the section */
					$this->addSection($section, $matches[2][$key]);

					/** remove the section from the body content */
					$this->bodyContent = str_replace($matches[0][$key], "", $this->bodyContent);
				}
			}

			/** start buffering again and process the layout */
			ob_start();

			/** process the view code */
			include_once $this->layout;

			/** get the output buffer contents */
			$pageContent = ob_get_contents();

			/** stop buffering */
			ob_end_clean();

			/** kill processing */
			die($pageContent);
		}

		/**
		 * Render the body content
		 * @return void
		 */
		public function renderBody()
		{
			print $this->bodyContent;
		}

		/**
		 * Render a defined section
		 * @param $section (string) - The name of the section to render
		 * @return void (void)
		 */
		public function renderSection($section)
		{
			if (isset($this->sections[$section])) print $this->sections[$section];
			else print "";
		}

		/**
		 * Add a defined section
		 * @param $section (string) - The name of the section to add
		 * @param $content (string) - The content of the section
		 * @return void (void)
		 */
		public function addSection($section, $content)
		{
			$this->sections[$section] = $content;
		}

		/**
		 * Show the error page (action)
		 * @return void
		 */
		public function ErrorPage()
		{
			/** Start buffering */
			ob_start();

			/** process the view code */
			include_once Config::get("viewsDirectory")."/shared/error.phtml";

			/** get the output buffer contents */
			$this->bodyContent = ob_get_contents();

			/** stop buffering */
			ob_end_clean();

			/** start buffering again and process the layout */
			ob_start();

			/** process the view code */
			include_once $this->layout;

			/** get the output buffer contents */
			$pageContent = ob_get_contents();

			/** stop buffering */
			ob_end_clean();

			/** kill processing */
			die($pageContent);
		}

		/**
		 * Show the error page
		 * @param \Exception $ex (Exception) - The exception that occurred
		 * @return void
		 */
		static function Exception(\Exception $ex)
		{
			/** get the headers */
			$headers = getHeaders();

			/** check the headers for a json call */
			if (isset($headers["Content-type"]) && $headers["Content-type"] == "application/json"){
				/** set the error response code */
				http_response_code(500);

				/** return a json object */
				MvcController::JsonException($ex);
			} else {
				/** create the dummy controller */
				$controller = new MvcController(new Route);
				$controller->viewBag["Exception"] = $ex;
				$controller->ErrorPage();
			}
		}

		/**
		 * Handle PHP errors as exceptions
		 * @param $no (int) - The error type
		 * @param $str (string) - The error description
		 * @param $file (string) - The file the error occurred in
		 * @param $line (int) - The line number of the file where the error occurred
		 * @return void
		 */
		static function Error($no, $str, $file, $line)
		{
			/** decide what type of error it is */
			$types = array(
				1 => "E_ERROR",
				2 => "E_WARNING",
				4 => "E_PARSE",
				8 => "E_NOTICE",
				16 => "E_CORE_ERROR",
				32 => "E_CORE_WARNING",
				64 => "E_COMPILE_ERROR",
				128 => "E_COMPILE_WARNING",
				256 => "E_USER_ERROR",
				512 => "E_USER_WARNING",
				1024 => "E_USER_NOTICE",
				6143 => "E_ALL",
				2048 => "E_STRICT",
				4096 => "E_RECOVERABLE_ERROR"
			);

			/** call the Exception method */
			MvcController::Exception(new \Exception($types[$no]." Caught: ".$str." in ".$file." on line ".$line));
		}
	}
}
?>