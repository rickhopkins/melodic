<?php
/**
 * @name Melodic\MVC\Controller
 * 		A base class for MVC and API controllers
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\MVC
{
	use Melodic\DI\Interfaces\iInject;
	use Melodic\DI\Personality\Injection;

	abstract class Controller implements iInject
	{
		/** use the Injection traits */
		use Injection;

		/** public properties */
		public $route;

		/**
		 * Initialize the controller and parse request
		 * @param Route $route (Melodic/MVC/Route) - The Route object to inform the controller
		 * @return \Melodic\MVC\Controller (Melodic\MVC\Controller) - The Controller object
		 */
		public function __construct(Route $route)
		{
			/** set variables */
			$this->route = $route;

			/** return the controller */
			return $this;
		}
		
		/**
		 * Execute the page
		 * @return void
		 */
		public function exec()
		{
			/** get the action */
			$action = $this->route->action;
			if (method_exists($this, $action)){
				/** create the parameters */
				$params = $this->route->params;
				foreach ($this->route->query as $key => $value) $params[$key] = $value;
				
				/** call the action and pass params */
				call_user_func_array(array($this, $action), $params);
			} else {
				/** send a page not found */
				pageNotFound();
			}
		}

		/**
		 * Return a json object
		 * @param $obj (mixed) - Data that you'd like returned as json
		 * @return void
		 */
		public function json($obj)
		{
			/** send content type header and json data */
			header("Content-type: application/json");
			die(json_encode($obj));
		}

		/**
		 * Return the exception message in json format
		 * @param \Exception $ex (Exception) - The exception that occurred
		 * @return void
		 */
		static function JsonException(\Exception $ex)
		{
			/** return the exception error message in json format and kill processing */
			header("Content-type: application/json");
			die(json_encode(array(
				"success" => false,
				"exception" => array(
					"message" => $ex->getMessage(),
					"code" => $ex->getCode(),
					"file" => $ex->getFile(),
					"line" => $ex->getLine(),
					"trace" => $ex->getTrace()
				)
			)));
		}
	}
}
?>