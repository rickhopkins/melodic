<?php
/**
 * @name Melodic\MVC\ApiController
 * 		Handles all requests for API requests
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\MVC
{
	use Melodic\DB\Model;

	abstract class ApiController extends Controller
	{
		/** private properties */
		public $model;

		/**
		 * Execute the call
		 */
		public function exec()
		{
			/** get the action */
			$action = $this->route->action;
			if (method_exists($this, $action)){
				/** check for a POST and map a Model to the data */
				if ($this->route->method == "POST" || $this->route->method == "PUT"){
					/** get the model data */
					$data = null;
					if ($this->route->method == "POST") $data = $_POST;
					else parse_str(file_get_contents('php://input'), $data);

					/** create the model */
					$modelInstance = new $this->model($data);

					/** add the model to the params */
					array_unshift($this->route->params, $modelInstance);
				}

				/** add url parameters to parameter list */
				if (array_key_exists("query", $this->route->query)) {
					/** check for a query */
					$this->route->params["query"] = $this->route->query["query"];
				} else {
					/** add each query param to the regular params */
					foreach ($this->route->query as $key => $value) $this->route->params[$key] = $value;
				}

				/** call the action and pass params */
				if ($action == "GetAll") $result = call_user_func(array($this, $action), $this->route->params);
				else $result = call_user_func_array(array($this, $action), $this->route->params);

				/** return the schools */
				$this->json($result);
			} else {
				/** send a page not found */
				pageNotFound();
			}
		}

		/**
		 * Get a specific record
		 * @param $id
		 * @return Model
		 */
		abstract public function Get($id);

		/**
		 * Get all records of type, or execute query
		 * @param array $query
		 * @return array
		 */
		abstract public function GetAll($query = array());

		/**
		 * Create a new record
		 * @param Model $model
		 * @return Model
		 */
		abstract public function Post(Model $model);

		/**
		 * Update a record
		 * @param Model $model
		 * @return Model
		 */
		abstract public function Put($id, Model $model);

		/**
		 * Delete a record
		 * @param $id
		 * @return void
		 */
		abstract public function Delete($id);
	}
}
?>