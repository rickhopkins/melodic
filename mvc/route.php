<?php
/**
 * @name Melodic\MVC\Route
 * 		Parses the current url to find the correct controller and action
 * @author Rick Hopkins
 * @package Melodic
 */

namespace Melodic\MVC
{
	/** using statements */
	use \Melodic\Config;

	class Route
	{
		/** public properties */
	    public $controller;
		public $action;
		public $vanity;
		public $method;
		public $request = array();
		public $params = array();
		public $query = array();
		public $isSPA = false;
		public $spaController;
		public $spaAction;
		public $spaOverrides = array();

		/**
		 * Initialize the Route object
		 * @param string $controller
		 * @param string $action
		 */
		public function __construct($controller = "home", $action = "index")
		{
			/** set controller and action */
			$this->controller = $controller;
			$this->action = $action;

			/** set properties */
			$this->method = strtolower($_SERVER["REQUEST_METHOD"]);
			parse_str($_SERVER["QUERY_STRING"], $this->query);
			
			/** set the vanity url */
			if (isset($_SERVER["REDIRECT_URL"])) $this->vanity = strtolower($_SERVER["REDIRECT_URL"]);
			else $this->vanity = "/".strtolower($this->controller."/".$this->action);

			/** set the request var */
			$this->request = explode("/", substr($this->vanity, 1), 3);
			if (!strlen($this->request[(count($this->request) - 1)]) > 0) array_pop($this->request);

			/** use the request to get the controller and action */
			if (isset($this->request[0])) $this->controller = $this->request[0];
			if (isset($this->request[1])) $this->action = $this->request[1];
			if (isset($this->request[2])) $this->params = explode("/", $this->request[2]);

			/** return Melodic\MVC\Route */
			return $this;
		}

		/**
		 * Configure a site for single page application
		 * @param string $spaController
		 * @param string $spaAction
		 * @param array $spaOverrides (url strings with controller & action. ex: /home/index)
		 */
		public function configureSPA($spaController = "home", $spaAction = "index", $spaOverrides = array())
		{
			/** set spa flag */
			$this->isSPA = true;

			/** set spa values */
			$this->spaController = $spaController;
			$this->spaAction = $spaAction;
			$this->spaOverrides = $spaOverrides;
		}
		
		/**
		 * Render the current path
		 * @return void
		 */
		public function render()
		{
			/** check for single page application */
			if (!Config::get("isAPI") && $this->isSPA){
				/** lowercase all overrides */
				while (list($key, $val) = each($this->spaOverrides)) {
					$this->spaOverrides[$key] = strtolower($val);
				}

				/** check overrides */
				$controllerAction = "/".strtolower($this->controller."/".$this->action);
				if (!in_array($controllerAction, $this->spaOverrides)){
					$this->controller = $this->spaController;
					$this->action = $this->spaAction;
				}
			}

			/** set the controller directory */
			$controllerDirectory = Config::get("controllerDirectory");

			/** check for isAPI */
			if (Config::get("isAPI")){
				/** adjust the controller name */
				$this->controller = $this->action;
				$this->action = ucfirst(strtolower($this->method));

				/** check for a third request item */
				if (count($this->request) < 3) $this->action .= "All";
				if (count($this->request) > 2 && str_contains($this->request[2], "/")){
					/** add onto action */
					$this->action .= ucfirst($this->params[0]);

					/** remove the first element in params */
					array_shift($this->params);
				}

				/** add the api root on the beginning of the controllers directory */
				$controllerDirectory = substr(Config::get("apiRoot"), 1)."\\".$controllerDirectory;
			}

			/** confirm the controller exists */
			if (file_exists(Config::get("controllerDirectory")."/".$this->controller.".php")){
				/** get the controller instance name, create, and execute */
				$controllerInstanceName = $controllerDirectory."\\".$this->controller;
				$controllerInstance = $controllerInstanceName::instantiateByInjection([$this]);
				$controllerInstance->exec();
			} else {
				/** send page not found */
				pageNotFound();
			}
		}
	}
}
?>